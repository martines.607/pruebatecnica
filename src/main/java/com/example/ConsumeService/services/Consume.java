package com.example.ConsumeService.services;

import com.example.ConsumeService.entities.ResponseSum;

import java.util.List;

public interface Consume {

    ResponseSum sendSum(Double numA , Double numB);


    List<ResponseSum> findAllSum();

}
