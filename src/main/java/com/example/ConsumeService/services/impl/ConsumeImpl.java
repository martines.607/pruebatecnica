package com.example.ConsumeService.services.impl;

import com.example.ConsumeService.entities.ResponseSum;
import com.example.ConsumeService.entities.Sum;
import com.example.ConsumeService.repositories.ResponseSumRepository;
import com.example.ConsumeService.repositories.SumRepository;
import com.example.ConsumeService.services.Consume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class ConsumeImpl implements Consume {

    private RestTemplate restTemplate;


    @Autowired
    private SumRepository sumRepository;
    @Autowired
    ResponseSumRepository responseSumRepository;

    @Override
    public ResponseSum sendSum(Double numA, Double numB) {

        Sum sum = new Sum(null ,numA,numA);

        var response = restTemplate.postForEntity("http://prueba-pedidos.persianassafra.com/safra/suma",
                sum, ResponseSum.class);


        var responseSumResult = response.getBody();
        if(responseSumResult != null){
            responseSumRepository.save(responseSumResult);
        }

        return responseSumResult;
    }

    @Override
    public List<ResponseSum> findAllSum() {
        return responseSumRepository.findAll();
    }


}
