package com.example.ConsumeService.controllers;

import com.example.ConsumeService.entities.ResponseSum;
import com.example.ConsumeService.services.Consume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/consume")
public class ConsumeController {


    @Autowired
    private Consume consume;


    @PostMapping("calculated/{numA}/{numB}")
    public ResponseEntity<ResponseSum> assignInformation (@PathVariable Double  numA, @PathVariable Double  numB, Model model) {
    model.addAttribute("numB" , numB);
    model.addAttribute("numA",numA);
    model.addAttribute("Titulo","Hola");
           return ResponseEntity.status(HttpStatus.OK).body(consume.sendSum(numA , numB));
    }


    @GetMapping("List")
    public ResponseEntity<List<ResponseSum>> assignInformation (Model model) {
        model.addAttribute("Titulo","Hola");
        return ResponseEntity.status(HttpStatus.OK).body(consume.findAllSum());
    }




}
