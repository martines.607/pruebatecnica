package com.example.ConsumeService.repositories;

import com.example.ConsumeService.entities.Sum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SumRepository extends MongoRepository<Sum , String> {
}
