package com.example.ConsumeService.repositories;

import com.example.ConsumeService.entities.ResponseSum;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResponseSumRepository extends MongoRepository<ResponseSum,String> {
}
